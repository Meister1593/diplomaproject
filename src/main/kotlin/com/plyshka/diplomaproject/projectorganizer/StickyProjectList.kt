package com.plyshka.diplomaproject.projectorganizer

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Button
import androidx.compose.material.Checkbox
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.plyshka.diplomaproject.projectorganizer.model.Project
import com.plyshka.diplomaproject.projectorganizer.model.ProjectType
import java.awt.Desktop
import java.nio.file.Paths

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun StickyProjectList(columnModifier: Modifier, projects: SnapshotStateList<Project>) {
    Box(
        modifier = columnModifier.fillMaxSize()
            .background(color = Color(180, 180, 180))
    ) {
        val scrollState = rememberLazyListState()
        LazyColumn(
            modifier = columnModifier
                .fillMaxHeight()
                .border(BorderStroke(2.dp, SolidColor(Color.DarkGray)))
                .height(200.dp)
                .padding(start = 5.dp),
            verticalArrangement = Arrangement.spacedBy(1.dp),
            state = scrollState
        ) {
            val borderModifier = Modifier.border(BorderStroke(2.dp, SolidColor(Color.DarkGray)))
                .padding(start = 3.dp, end = 3.dp)
            ProjectType.values().forEach { projectType ->
                val filteredProjects = projects.filter { it.type == projectType }
                stickyHeader {
                    Row(
                        modifier = Modifier.height(IntrinsicSize.Min)
                            .fillMaxWidth()
                            .background(Color(red = 0.5f, green = 0.5f, blue = 0.5f))
                            .alpha(0.95f)
                    ) {
                        val checkedAllProjects = remember { mutableStateOf(false) }
                        if (checkedAllProjects.value && filteredProjects.isEmpty()) {
                            checkedAllProjects.value = false
                        }
                        Checkbox(
                            modifier = borderModifier,
                            checked = checkedAllProjects.value,
                            onCheckedChange = { changedState ->
                                checkedAllProjects.value = changedState
                                projects.filter { it.type == projectType }
                                    .forEach {
                                        it.checkedMove.value = changedState
                                    }
                            })
                        val painter = painterResource(resourcePath = projectType.iconResourcePath)
                        Image(
                            painter = painter,
                            contentDescription = null
                        )
                        Text(fontSize = 18.sp, text = projectType.appendedText)
                    }
                }
                items(filteredProjects.size) { index ->
                    val project = filteredProjects[index]
                    val rowSize = remember { mutableStateOf(IntSize.Zero) }
                    Row(
                        modifier = Modifier.height(IntrinsicSize.Max)
                            .onSizeChanged { rowSize.value = it },
                    ) {
                        Checkbox(
                            modifier = borderModifier,
                            checked = project.checkedMove.value,
                            onCheckedChange = { project.checkedMove.value = it })
                        SelectionContainer {
                            Column(
                                modifier = Modifier.height(IntrinsicSize.Max)
                                    .padding(5.dp)
                            ) {
                                Text(
                                    modifier = Modifier
                                        .wrapContentHeight(align = Alignment.CenterVertically),
                                    textAlign = TextAlign.Left,
                                    fontWeight = FontWeight.Bold,
                                    text = when (projectType.isSourceCode) {
                                        true -> project.path
                                        false -> project.name
                                    },
                                )
                                if (!project.type.isSourceCode) {
                                    Text(
                                        modifier = Modifier.width(450.dp)
                                            .wrapContentHeight(align = Alignment.CenterVertically),
                                        textAlign = TextAlign.Left,
                                        text = project.path,
                                        fontSize = 12.sp,
                                        overflow = TextOverflow.Ellipsis,
                                        maxLines = 1,
                                    )
                                }
                            }
                        }
                        Spacer(Modifier.weight(1f))
                        Button(onClick = {
                            val directoryFile = when (projectType.isSourceCode) {
                                true -> Paths.get(project.path).toFile().parentFile
                                false -> Paths.get(project.path).toFile()
                            }
                            Desktop.getDesktop().open(directoryFile)
                        }) {
                            Text(text = "Open folder")
                        }
                    }
                }
            }
        }
        VerticalScrollbar(
            adapter = rememberScrollbarAdapter(
                scrollState = scrollState
            ),
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            style = ScrollbarStyle(
                minimalHeight = 16.dp,
                thickness = 15.dp,
                shape = RoundedCornerShape(4.dp),
                hoverDurationMillis = 300,
                unhoverColor = Color.Black.copy(alpha = 0.4f),
                hoverColor = Color.Black.copy(alpha = 0.90f)
            )
        )
    }
}