package com.plyshka.diplomaproject.projectorganizer

import com.plyshka.diplomaproject.projectorganizer.model.Project
import com.plyshka.diplomaproject.projectorganizer.model.ProjectType
import kotlinx.coroutines.flow.flow
import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import java.time.LocalDateTime
import kotlin.io.path.absolutePathString
import kotlin.io.path.isSameFileAs

suspend fun lookForProjects(searchPath: String) = flow<Project> {
    var lastProjectPath: Path? = null
    var foundProject = false
    File(searchPath).walk().forEach {
        if (!it.isDirectory || lastProjectPath?.isSameFileAs(it.toPath()) == true) {
            return@forEach
        }
        val directory = it
        val directoryPath = directory.toPath()
        if (foundProject) {
            if (isPathUnderneath(lastProjectPath!!, directoryPath)) {
                return@forEach
            }
            foundProject = false
        }
        val currentFolderFiles = it.listFiles()
        currentFolderFiles?.forEach folderIterator@{ file ->
            if (file.isDirectory) {
                return@folderIterator
            }
            var projectName: String? = null
            var projectFolder: String? = null
            val projectType = ProjectType.values().firstOrNull { type ->
                if (type.isSourceCode && file.name.endsWith(type.fileNameContaining)) {
                    projectName = file.name
                    projectFolder = file.absolutePath
                    return@firstOrNull true
                }
                if (type.fileNameExact && file.name.equals(type.fileNameContaining)
                    || (!type.fileNameExact && file.name.contains(type.fileNameContaining))
                ) {
                    projectName = directory.name
                    projectFolder = directory.absolutePath
                    return@firstOrNull true
                }
                return@firstOrNull false
            } ?: return@folderIterator
            emit(
                Project(
                    type = projectType,
                    path = projectFolder!!,
                    name = projectName!!,
                )
            )
            if (projectType.isSourceCode) {
                return@folderIterator
            }
            foundProject = true
            lastProjectPath = directory.toPath()
        }
    }
}

suspend fun moveProjects(pickedFilePathForMoving: Path, projectList: List<Project>) = flow<Project> {
    projectList.forEach {
        if (!it.checkedMove.value) {
            return@forEach
        }
        val finalPath = Paths.get(
            pickedFilePathForMoving.absolutePathString(),
            it.type.projectFolderName,
            it.name
        )
        try {
            val projectPath = Paths.get(it.path)
            if (!it.type.isSourceCode) {
                FileUtils.copyDirectory(projectPath.toFile(), finalPath.toFile())
            } else {
                FileUtils.copyFile(projectPath.toFile(), finalPath.toFile())
            }
        } catch (e: FileAlreadyExistsException) {
            println("File path exists, skipping for $it")
            emit(it)
            return@forEach
        }
        it.moved.value = true
        emit(it)
    }
}

suspend fun removeMovedProjects(projectList: List<Project>) = flow<Project> {
    projectList.forEach {
        val projectPath = Paths.get(it.path)
        projectPath.toFile().deleteRecursively()
        emit(it)
    }
}

fun generateHtmlDocumentForProjects(pickedProjectsFolderForMove: Path?, projectList: List<Project>) {
    val current = LocalDateTime.now()
    val htmlString = buildString {
        appendHTML().html {
            body {
                h1 {
                    +"Projects sheet"
                }
                p {
                    +"Organized at $current"
                }
                projectList.forEach {
                    h2 {
                        +it.name
                    }
                    p {
                        +"Project type: ${it.type.projectFolderName}"
                    }
                    p {
                        +"Source path: ${it.path}"
                    }
                }
            }
        }
    }
    val finalPath = Paths.get(
        pickedProjectsFolderForMove!!.absolutePathString(),
        "ProjectsSheet.html"
    )
    finalPath.toFile().writeText(htmlString)
}

fun isPathUnderneath(parentPath: Path, childPath: Path): Boolean {
    return childPath.startsWith(parentPath.normalize().toAbsolutePath())
}