import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import com.plyshka.diplomaproject.projectorganizer.*
import com.plyshka.diplomaproject.projectorganizer.model.Project
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import javax.swing.JFileChooser

private val Gray56 = Color(0xff202020)

@OptIn(ExperimentalMaterialApi::class)
@Composable
@Preview
fun App() {
    val modifier = Modifier.fillMaxSize()
    val findingProjectsProgress = remember { mutableStateOf(false) }
    val movingProjectsProgress = remember { mutableStateOf(false) }
    MaterialTheme(colors = MaterialTheme.colors.copy(primary = Gray56)) {
        Column(
            modifier.border(BorderStroke(2.dp, SolidColor(Color.DarkGray)))
                .background(Color.LightGray)
        ) {
            Text(
                modifier = Modifier.border(BorderStroke(2.dp, SolidColor(Color.DarkGray)))
                    .padding(5.dp)
                    .fillMaxWidth(),
                text = "Project Organizer",
                fontSize = 17.sp,
                textAlign = TextAlign.Center,
            )
            var pickedFilePathForSearch by remember {
                mutableStateOf<File>(
                    Paths.get("").toFile()
                )
            }
            val projects = remember { mutableStateListOf<Project>() }
            val moveJob: MutableState<Job?> = remember { mutableStateOf(null) }
            Row(
                modifier = Modifier.padding(5.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                SelectionContainer(
                    Modifier.wrapContentHeight(align = Alignment.CenterVertically)
                ) {
                    Text(
                        modifier = Modifier.width(450.dp)
                            .padding(5.dp),
                        text = "Current search path: \"${pickedFilePathForSearch.absolutePath}\"",
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                    )
                }

                Spacer(Modifier.weight(1f))
                val projectsReadyForDeletion = remember { mutableStateListOf<Project>() }
                val openDeleteSourceDialog = remember { mutableStateOf(false) }
                val openGenerateDocumentationDialog = remember { mutableStateOf(false) }
                val noCheckedProjectsDialog = remember { mutableStateOf(false) }
                val pickedProjectsFolderForMove = remember { mutableStateOf<Path?>(null) }
                if (projects.isNotEmpty()) {
                    val scope = rememberCoroutineScope()
                    Button(onClick = {
                        if (projects.none { it.checkedMove.value }) {
                            noCheckedProjectsDialog.value = true
                        } else {
                            val pickedFile = openFileDialog(JFileChooser.DIRECTORIES_ONLY) ?: return@Button
                            pickedProjectsFolderForMove.value = pickedFile.toPath()

                            moveJob.value = scope.launch(Dispatchers.Default) {
                                movingProjectsProgress.value = true
                                moveProjects(pickedProjectsFolderForMove.value!!, projects.toList()).cancellable()
                                    .collect {
                                        if (it.moved.value) {
                                            projectsReadyForDeletion.add(it)
                                            projects.remove(it)
                                        } else {
                                            println("could not move project $it")
                                        }
                                    }
                                movingProjectsProgress.value = false
                                openDeleteSourceDialog.value = true
                            }
                        }
                    }) {
                        Text("Start moving projects ->")
                    }
                }
                if (openDeleteSourceDialog.value) {
                    AlertDialog(
                        modifier = Modifier.padding(8.dp).width(250.dp),
                        onDismissRequest = {
                            openDeleteSourceDialog.value = false
                            openGenerateDocumentationDialog.value = true
                        },
                        title = {
                            Text(text = "Projects has been copied.\nDelete source projects?")
                        },
                        buttons = {
                            val scope = rememberCoroutineScope()
                            Button(
                                modifier = Modifier.fillMaxWidth(),
                                onClick = {
                                    scope.launch {
                                        removeMovedProjects(projectsReadyForDeletion).collect {
                                            println("removed ${it.path}")
                                        }
                                        openDeleteSourceDialog.value = false
                                        openGenerateDocumentationDialog.value = true
                                    }
                                }
                            ) {
                                Text("Yes")
                            }
                            Button(
                                modifier = Modifier.fillMaxWidth(),
                                onClick = {
                                    openDeleteSourceDialog.value = false
                                    openGenerateDocumentationDialog.value = true
                                }
                            ) {
                                Text("No")
                            }
                        }
                    )
                }
                if (openGenerateDocumentationDialog.value) {
                    AlertDialog(
                        modifier = Modifier.padding(8.dp).width(350.dp),
                        onDismissRequest = {
                            openDeleteSourceDialog.value = false
                        },
                        title = {
                            Text(text = "Would you like to create small projects sheet page in moved projects directory?")
                        },
                        buttons = {
                            val scope = rememberCoroutineScope()
                            Button(
                                modifier = Modifier.fillMaxWidth(),
                                onClick = {
                                    scope.launch {
                                        generateHtmlDocumentForProjects(
                                            pickedProjectsFolderForMove.value,
                                            projectsReadyForDeletion
                                        )
                                        projectsReadyForDeletion.clear()
                                        openGenerateDocumentationDialog.value = false
                                    }
                                }
                            ) {
                                Text("Yes")
                            }
                            Button(modifier = Modifier.fillMaxWidth(), onClick = {
                                projectsReadyForDeletion.clear()
                                openGenerateDocumentationDialog.value = false
                            }) {
                                Text("No")
                            }
                        }
                    )
                }
                if (noCheckedProjectsDialog.value) {
                    AlertDialog(
                        modifier = Modifier.padding(8.dp).width(250.dp),
                        onDismissRequest = {
                            noCheckedProjectsDialog.value = false
                        },
                        title = {
                            Text(text = "Check at least one project to start moving")
                        },
                        buttons = {
                            Button(
                                modifier = Modifier.fillMaxWidth(),
                                onClick = {
                                    noCheckedProjectsDialog.value = false
                                }
                            ) {
                                Text("Ok")
                            }
                        }
                    )
                }
            }
            if (movingProjectsProgress.value) {
                ProgressIndicatorWithButton(
                    text = "Waiting for project move completion",
                    cancellationJob = moveJob.value,
                    progressState = movingProjectsProgress
                )
            }
            Row(modifier.border(BorderStroke(2.dp, SolidColor(Color.DarkGray)))) {
                StickyProjectList(Modifier.weight(0.8f), projects)
                Column(
                    modifier = Modifier.weight(0.2f)
                        .border(BorderStroke(2.dp, SolidColor(Color.Gray)))
                        .padding(horizontal = 5.dp, vertical = 5.dp),
                    verticalArrangement = Arrangement.spacedBy(5.dp)
                ) {
                    Button(modifier = Modifier.fillMaxWidth(), onClick = {
                        val pickedDirectory = openFileDialog(JFileChooser.FILES_AND_DIRECTORIES) ?: return@Button
                        pickedFilePathForSearch = pickedDirectory
                    }) {
                        Text("Select folder to start search")
                    }
                    val searchJob: MutableState<Job?> = remember { mutableStateOf(null) }
                    if (findingProjectsProgress.value) {
                        ProgressIndicatorWithButton(
                            text = "Waiting for project search completion",
                            cancellationJob = searchJob.value,
                            progressState = findingProjectsProgress
                        )
                    }
                    val scope = rememberCoroutineScope()
                    Button(modifier = Modifier.fillMaxWidth(), onClick = {
                        searchJob.value = scope.launch(Dispatchers.Default) {
                            findingProjectsProgress.value = true
                            lookForProjects(pickedFilePathForSearch.absolutePath).cancellable().collect {
                                projects.add(it)
                            }
                            findingProjectsProgress.value = false
                        }
                    }) {
                        Text("Start searching projects")
                    }
                    Button(modifier = Modifier.fillMaxWidth(), onClick = {
                        projects.clear()
                    }) {
                        Text("Clear projects list")
                    }
                }
            }
        }
    }
}

private fun openFileDialog(fileSelectionMode: Int): File? {
    val chooser = JFileChooser()
    chooser.fileSelectionMode = fileSelectionMode
    return when (chooser.showSaveDialog(null)) {
        JFileChooser.CANCEL_OPTION -> null
        JFileChooser.ERROR_OPTION -> null
        else -> chooser.selectedFile
    }
}

fun main() = application {
    Window(onCloseRequest = ::exitApplication, title = "Project Organizer") {
        App()
    }
}

@Composable
fun ProgressIndicatorWithButton(
    text: String,
    cancellationJob: Job?,
    progressState: MutableState<Boolean>
) {
    Column(
        modifier = Modifier.border(BorderStroke(2.dp, SolidColor(Color.DarkGray)))
            .padding(5.dp)
    ) {
        Text(
            text = text,
            fontSize = 18.sp,
            modifier = Modifier.align(Alignment.CenterHorizontally)
        )
        CircularProgressIndicator(modifier = Modifier.align(Alignment.CenterHorizontally))
        Button(modifier = Modifier.fillMaxWidth(), onClick = {
            cancellationJob!!.cancel()
            progressState.value = false
        }) {
            Text("Abort")
        }
    }
}