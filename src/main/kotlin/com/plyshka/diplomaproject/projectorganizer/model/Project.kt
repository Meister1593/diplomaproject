package com.plyshka.diplomaproject.projectorganizer.model

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

enum class ProjectType(
    val iconResourcePath: String,
    val appendedText: String,
    val isSourceCode: Boolean,
    val fileNameContaining: String,
    val fileNameExact: Boolean,
    val projectFolderName: String,
) {
    GRADLE(
        iconResourcePath = "gradle-icon.svg",
        appendedText = " - Gradle",
        isSourceCode = false,
        fileNameContaining = "build.gradle",
        projectFolderName = "Gradle",
        fileNameExact = false,
    ),
    MAVEN(
        iconResourcePath = "maven-icon.svg",
        appendedText = " - Maven",
        isSourceCode = false,
        fileNameContaining = "pom.xml",
        projectFolderName = "Maven",
        fileNameExact = true,
    ),
    NPM(
        iconResourcePath = "npm-icon.svg",
        appendedText = " - NPM",
        isSourceCode = false,
        fileNameContaining = "package.json",
        projectFolderName = "NPM",
        fileNameExact = true,
    ),
    RUST_LANG(
        iconResourcePath = "rust-lang-icon.svg",
        appendedText = " - Rust",
        isSourceCode = true,
        fileNameContaining = ".rs",
        projectFolderName = "Rust",
        fileNameExact = true,
    ),
    RUST_LANG_CARGO(
        iconResourcePath = "rust-lang-icon.svg",
        appendedText = " - Rust Cargo",
        isSourceCode = false,
        fileNameContaining = "Cargo.toml",
        projectFolderName = "Cargo-Rust",
        fileNameExact = true,
    )
}

open class Project(
    open val type: ProjectType,
    open val path: String,
    open val name: String,
    open var checkedMove: MutableState<Boolean> = mutableStateOf(false),
    open var moved: MutableState<Boolean> = mutableStateOf(false),
)

